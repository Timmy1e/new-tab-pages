/*jshint esversion: 6 */
(() => {
	const greetings = ['おやすみなさい', 'おはようございます', 'こんにちは', 'こんばんは'];
	const elmGreeting = document.getElementById('greeting');
	const elmHour = document.getElementById('hour');
	const elmMinute = document.getElementById('minute');
	const elmDayTime = document.getElementById('dayTime');
	const elmYear = document.getElementById('year');
	const elmMonth = document.getElementById('month');
	const elmDay = document.getElementById('day');
	const elmDayOfWeek = document.getElementById('dayOfWeek');
	const dateTimeFormat = new Intl.DateTimeFormat('ja-JP', {
		year: 'numeric',
		month: 'numeric',
		day: 'numeric',
		weekday: 'narrow'
	});

	(function updateDateTime() {
		let now = new Date();
		let hours = now.getHours();
		let minutes = now.getMinutes();
		let dayPart = Math.floor(hours / 6);
		let dayTime = hours >= 12 ? '午後' : '午前';
		hours = hours % 12;
		hours = hours ? hours : 12;
		minutes = (minutes < 10 ? '0' : '') + minutes;

		const nowFormat = dateTimeFormat.formatToParts(now).reduce((prev, curr) => {
			prev[curr.type] = curr.value;
			return prev;
		}, {});

		elmGreeting.innerText = greetings[dayPart];
		elmHour.innerText = hours;
		elmMinute.innerText = minutes;
		elmDayTime.innerText = dayTime;
		elmYear.innerText = nowFormat.year;
		elmMonth.innerText = nowFormat.month;
		elmDay.innerText = nowFormat.day;
		elmDayOfWeek.innerText = nowFormat.weekday;
		setTimeout(updateDateTime.bind(this), 1000);
	})();
})();
