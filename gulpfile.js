/*jshint esversion: 6, node: true */
const q = require('q');
const fs = require('fs');
const path = require('path');
const gulp = require('gulp');
const sjcl = require('sjcl');
const sass = require('gulp-sass');
const bump = require('gulp-bump');
const clean = require('gulp-clean');
const gUtil = require('gulp-util');
const merge = require('merge-stream');
const uglify = require('gulp-uglify-es').default;
const inject = require('gulp-inject-string');
const rename = require('gulp-rename');
const imageResize = require('gulp-image-resize');

const DIR = {
	OUTPUT: 'out',
	SOURCE: 'src',
	NODE: 'node_modules'
};
const FILE = {
	TEMPLATE: {
		PAGE: path.join(DIR.SOURCE, 'page.html'),
		TOP: path.join(DIR.SOURCE, 'top.html'),
		ICON: path.join(DIR.SOURCE, 'icon.html')
	},
	STYLE: path.join(DIR.SOURCE, 'style.scss'),
	SCRIPT: path.join(DIR.SOURCE, 'script.js'),
	CHROME: path.join(DIR.SOURCE, 'chrome.json'),
	CONFIG: 'config.json',
	PACKAGE: 'package.json'
};
const FONTS = [
	{
		style: {
			in: path.join(DIR.NODE, 'typeface-open-sans', 'index.css'),
			out: 'open-sans.css'
		},
		dir: {
			in: path.join(DIR.NODE, 'typeface-open-sans', 'files'),
			out: 'fonts'
		}
	}, {
		style: {
			in: path.join(DIR.NODE, 'font-awesome', 'css', 'font-awesome.min.css'),
			out: 'font-awesome.css'
		},
		dir: {
			in: path.join(DIR.NODE, 'font-awesome', 'fonts'),
			out: 'fonts'
		}
	}
];

function injectRemove(str) {
	return inject.replace(str, '');
}

function readFile(fileName) {
	let defer = q.defer();
	fs.readFile(fileName, 'utf8', (err, data) => {
		if (err) {
			defer.reject(err);
		} else {
			defer.resolve(data);
		}
	});
	return defer.promise;
}

gulp.task('default', ['build']);

gulp.task('clean', () => gulp.src('out', {read: false}).pipe(clean()));

gulp.task('build', ['clean'], () => {
	let config = {};
	let injectPipe;

	/* --- Config --- */
	gUtil.log(gUtil.colors.green('+'), 'Read config');
	return readFile(FILE.CONFIG)
		.then((data) => {
			config = JSON.parse(data);
		})
		/* -/- Config -/- */


		/* --- Style --- */
		.then(() => {
			gUtil.log(gUtil.colors.green('+'), 'Compile style');
			let defer = q.defer();
			gulp.src(FILE.STYLE)
				.pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
				.pipe(gulp.dest(DIR.OUTPUT))
				.on('end', defer.resolve);
			return defer.promise;
		})
		/* -/- Style -/- */


		/* --- Script --- */
		.then(() => {
			gUtil.log(gUtil.colors.green('+'), 'Compress script');
			let defer = q.defer();
			gulp.src(FILE.SCRIPT)
				.pipe(uglify())
				.pipe(rename('script.min.js'))
				.pipe(gulp.dest(DIR.OUTPUT))
				.on('end', defer.resolve);
			return defer.promise;
		})
		/* -/- Script -/- */

		/* --- Fonts --- */
		.then(() => {
			gUtil.log(gUtil.colors.green('+'), 'Copy fonts');

			let promises = [];
			FONTS.forEach(font => {
				const fontStyleDefer = q.defer();
				gulp.src(font.style.in)
					.pipe(inject.replace('files/', '/fonts/'))
					.pipe(inject.replace('../fonts/', 'fonts/'))
					.pipe(rename(font.style.out))
					.pipe(gulp.dest(DIR.OUTPUT))
					.on('end', fontStyleDefer.resolve);
				promises.push(fontStyleDefer.promise);

				const fontDirDefer = q.defer();
				gulp.src([path.join(font.dir.in, '**')])
					.pipe(gulp.dest(path.join(DIR.OUTPUT, font.dir.out)))
					.on('end', fontDirDefer.resolve);
				promises.push(fontDirDefer.promise);

			});
			return q.all(promises);
		})
		/* -/- Fonts -/- */


		/* --- Chrome --- */
		.then(() => {
			gUtil.log(gUtil.colors.green('+'), 'Copy Chrome manifest');

			const chromeDefer = q.defer();
			gulp.src(FILE.CHROME)
				.pipe(rename('manifest.json'))
				.pipe(gulp.dest(DIR.OUTPUT))
				.on('end', chromeDefer.resolve);
			return chromeDefer.promise;
		})
		/* -/- Chrome -/- */


		/* --- Hash --- */
		.then(() => {
			gUtil.log(gUtil.colors.green('+'), 'Read script');

			return readFile(path.join(DIR.OUTPUT, 'script.min.js'));
		})
		.then(scriptData => {
			gUtil.log(gUtil.colors.green('+'), 'Calculate script hash');

			const defer = q.defer();
			const hashBits = sjcl.hash.sha256.hash(scriptData);
			const hashValue = sjcl.codec.base64.fromBits(hashBits);
			defer.resolve(hashValue);
			return defer.promise;
		})
		/* -/- Hash -/- */


		/* --- HTML --- */
		.then(scriptHash => {
			gUtil.log(gUtil.colors.green('+'), 'Build templates');

			injectPipe = gulp
				.src(FILE.TEMPLATE.PAGE)
				.on('error', console.error)
				.pipe(inject.replace('<SHA>', scriptHash));

			gUtil.log(gUtil.colors.grey('  -'), 'Read template top');

			return readFile(FILE.TEMPLATE.TOP);
		})
		.then(templateTop => {
			//TODO workaround, the read file doesn't work
			templateTop = '<a class="{color}" href="{link}">{title}</a>\n';
			gUtil.log(gUtil.colors.grey('  -'), 'Build templates top');

			if (Array.isArray(config.top)) {
				config.top.forEach((top, index) => {
					injectPipe = injectPipe.pipe(
						inject.replace(
							'{top_' + index + '}',
							templateTop
								.replace('{color}', top.color)
								.replace('{link}', top.link)
								.replace('{title}', top.title) +
							'{top_' + (index + 1) + '}'
						)
					)
				});
				injectPipe = injectPipe.pipe(injectRemove('{top_' + config.top.length + '}'));
			}

			gUtil.log(gUtil.colors.grey('  -'), 'Read template icon');
			//
			// return readFile(FILE.TEMPLATE.ICON);
		})
		.then(templateIcon => {
			//TODO workaround, the read file doesn't work
			templateIcon = '<a class="fa {icon} {color}" href="{link}"></a>\n';
			gUtil.log(gUtil.colors.grey('  -'), 'Build templates icon');

			if (Array.isArray(config.icons)) {
				config.icons.forEach((icon, index) => {
					injectPipe = injectPipe.pipe(
						inject.replace(
							'{icon_' + index + '}',
							templateIcon
								.replace('{color}', icon.color)
								.replace('{link}', icon.link)
								.replace('{icon}', icon.icon) +
							'{icon_' + (index + 1) + '}'
						)
					)
				});
				injectPipe = injectPipe.pipe(injectRemove('{icon_' + config.icons.length + '}'));
			}

			gUtil.log(gUtil.colors.grey('  -'), 'Minify templates');

			let defer = q.defer();
			injectPipe
				.pipe(injectRemove('\t'))
				.pipe(inject.replace('\n', ' '))
				.pipe(rename('newtab.html'))
				.pipe(gulp.dest(DIR.OUTPUT))
				.on('end', defer.resolve)
				.on('error', defer.reject);
			return defer.promise;
		})
		.catch(reason => gUtil.log(gUtil.colors.red('!'), reason));
	/* -/- HTML -/- */
});

gulp.task('bump::major', () => {
	return merge([
		gulp.src(FILE.CHROME)
			.pipe(bump({type: 'major'}))
			.pipe(gulp.dest(DIR.SOURCE)),
		gulp.src(FILE.PACKAGE)
			.pipe(bump({type: 'major'}))
			.pipe(gulp.dest(''))
	]);
});

gulp.task('bump::minor', () => {
	return merge([
		gulp.src(FILE.CHROME)
			.pipe(bump({type: 'minor'}))
			.pipe(gulp.dest(DIR.SOURCE)),
		gulp.src(FILE.PACKAGE)
			.pipe(bump({type: 'minor'}))
			.pipe(gulp.dest(''))
	]);
});

gulp.task('bump::patch', () => {
	return merge([
		gulp.src(FILE.CHROME)
			.pipe(bump({type: 'patch'}))
			.pipe(gulp.dest(DIR.SOURCE)),
		gulp.src(FILE.PACKAGE)
			.pipe(bump({type: 'patch'}))
			.pipe(gulp.dest(''))
	]);
});
