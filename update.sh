#!/usr/bin/env sh

set -e

dir=$(dirname "$0")
printf "${MARKER_2} Moving working directory to '${dir}'\n"
cd "${dir}"

branch=$(git rev-parse --symbolic-full-name --abbrev-ref @{upstream} | sed 's!/! !')
printf "${MARKER_2} Fetching "
git fetch ${branch}

printf "${MARKER_2} Checking GIT\n"
if [ $(git rev-parse HEAD) = $(git rev-parse @{u}) ]; then
	printf "${MARKER_3} Already up-to-date\n"
else
	printf "${MARKER_3} Pullling using git\n"
	git pull ${branch}

	printf "${MARKER_3} Installing using npm\n"
	npm install --production

	printf "${MARKER_3} Building build using gulp\n"
	./node_modules/.bin/gulp build
fi

printf "${MARKER_2_SUCCESS} Done!\n"
exit 0

